## 0.4.1 (December 14, 2022)
  - Hotfix - CK-320 - Retry Handler bug fixes

## 0.4.0 (December 06, 2022)
  - Feature - CK-320 - Add retry handler middleware

## 0.3.2 (December 02, 2022)
  - Bugfix - CK-320 - Fixed 401 Unauthorized exception response

## 0.3.1 (April 13, 2022)
  - Bugfix - CK-163 - Fixed base_url constructor option

## 0.3.0 (April 13, 2022)
  - Feature - CK-163 - Upgraded guzzle to version 7

## 0.2.0 (October 26, 2021)
  - Change package name with hprotravel-compass/php-gcp-rest-client

## 0.1.1 (April 04, 2019)
  - Change branch alias 1.0.x-dev to 0.1.x-dev

## 0.1.0 (April 04, 2019)
  - Apcu method bug is fixed
  - Token bug is fixed
  - .gitignore added
  - minor package fix
  - initial commit

