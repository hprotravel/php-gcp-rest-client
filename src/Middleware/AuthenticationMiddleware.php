<?php

namespace GcpRestGuzzleAdapter\Middleware;

use GcpRestGuzzleAdapter\Authentication\Credential;
use GcpRestGuzzleAdapter\Authentication\TokenProvider;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class AuthenticationMiddleware
{
    /**
     * @var Credential
     */
    protected $credential;

    /**
     * @var TokenProvider
     */
    protected $provider;

    /**
     * AuthenticationMiddleware constructor.
     * @param Credential $credential
     * @param TokenProvider $provider
     */
    public function __construct(Credential $credential, TokenProvider $provider)
    {
        $this->credential = $credential;
        $this->provider = $provider;
    }

    public function __invoke(callable $handler)
    {
        return function (RequestInterface $request, array $options) use ($handler) {
            return $handler($this->configureAuthorization($request), $options)->then(
                function (ResponseInterface $response) use ($handler, $request, $options) {
                    if ($response->getStatusCode() === 401) {
                        return $handler($this->refreshAuthorization($request), $options);
                    }

                    return $response;
                }
            );
        };
    }

    /**
     * @param RequestInterface $request
     * @return RequestInterface
     * @throws GuzzleException
     */
    private function configureAuthorization(RequestInterface $request)
    {
        $token = $this->provider->getToken($this->credential);

        return $request->withHeader(
            'Authorization',
            sprintf('%s %s', $token['token_type'], $token['access_token'])
        );
    }

    /**
     * @param RequestInterface $request
     * @return RequestInterface
     * @throws GuzzleException
     */
    private function refreshAuthorization(RequestInterface $request)
    {
        $this->provider->clearToken($this->credential);

        return $this->configureAuthorization($request);
    }
}
