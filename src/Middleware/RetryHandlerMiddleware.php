<?php

namespace GcpRestGuzzleAdapter\Middleware;

use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Promise\Create;
use GuzzleHttp\Promise\PromiseInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class RetryHandlerMiddleware
{
    /**
     * @var int
     */
    protected $maxRetryAttempts;

    /**
     * @var callable(RequestInterface, array): PromiseInterface
     */
    protected $nextHandler;

    /**
     * RetryHandlerMiddleware constructor.
     * @param int $maxRetryAttempts
     */
    public function __construct(int $maxRetryAttempts)
    {
        $this->maxRetryAttempts = $maxRetryAttempts;
    }

    public function __invoke(callable $handler)
    {
        $this->nextHandler = $handler;

        return function (RequestInterface $request, array $options): PromiseInterface {
            if (!isset($options['retry_attempts'])) {
                $options['retry_attempts'] = 0;
            }

            return $this->createPromise($request, $options);
        };
    }

    private function createPromise(RequestInterface $request, array $options): PromiseInterface
    {
        $handler = $this->nextHandler;

        return $handler($request, $options)->then(
            $this->onFulfilled($request, $options),
            $this->onRejected($request, $options)
        );
    }

    private function onFulfilled(RequestInterface $request, array $options): callable
    {
        return function (ResponseInterface $response) use ($request, $options) {
            return $response;
        };
    }

    private function onRejected(RequestInterface $request, array $options): callable
    {
        return function (\Throwable $reason) use ($request, $options) {
            if (!$this->isExceedMaxRetryAttempts($options['retry_attempts'])) {
                // Retry connection exceptions
                if ($reason instanceof ConnectException) {
                    return $this->doRetry($request, $options);
                }

                // Retry on server errors
                if ($reason instanceof ServerException) {
                    return $this->doRetry($request, $options);
                }
            }

            return Create::rejectionFor($reason);
        };
    }

    private function isExceedMaxRetryAttempts(int $retryAttempts): bool
    {
        return $retryAttempts >= $this->maxRetryAttempts;
    }

    private function exponentialDelay(int $retryAttempts): int
    {
        return (int)\pow(2, $retryAttempts - 1) * 1000;
    }

    private function doRetry(RequestInterface $request, array $options): PromiseInterface
    {
        $options['delay'] = $this->exponentialDelay(++$options['retry_attempts']);

        return $this->createPromise($request, $options);
    }
}
