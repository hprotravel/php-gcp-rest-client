<?php

namespace GcpRestGuzzleAdapter\Authentication;

use Firebase\JWT\JWT;

class Credential
{
    /**
     * client_email value in service account file
     * @var string
     */
    protected $email;

    /**
     * private_key value in service account file
     *
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $scope;

    /**
     * Credential constructor.
     * @param string $email
     * @param string $key
     * @param string $scope
     */
    public function __construct($email, $key, $scope)
    {
        $this->email = $email;
        $this->key = $key;
        $this->scope = $scope;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Credential
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     * @return Credential
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return string
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @param string $scope
     * @return Credential
     */
    public function setScope($scope)
    {
        $this->scope = $scope;
        return $this;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function createJwtToken()
    {
        $assertion = [
            "iss" => $this->getEmail(),
            "aud" => "https://www.googleapis.com/oauth2/v4/token",
            "iat" => (new \DateTime())->getTimestamp(),
            "exp" => (new \DateTime('+60 minutes'))->getTimestamp(),
            "scope" => $this->getScope(),
        ];

        return JWT::encode($assertion, $this->getKey(), 'RS256');
    }

    /**
     * @return string
     */
    public function createCacheKey()
    {
        return md5($this->getEmail() . $this->getKey() . $this->getScope());
    }
}