<?php

namespace GcpRestGuzzleAdapter\Authentication;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GcpRestGuzzleAdapter\Cache\CacheInterface;

class TokenProvider
{
    /**
     * Token URL of Google Class Authentication endpoint
     */
    const TOKEN_URL = 'https://www.googleapis.com/oauth2/v4/token';

    /**
     * OAUTH 2 grant type
     */
    const GRANT_TYPE = 'urn:ietf:params:oauth:grant-type:jwt-bearer';

    /**
     * @var Client
     */
    protected $tokenClient;

    /**
     * @var CacheInterface
     */
    protected $cacheClient;

    /**
     * TokenProvider constructor.
     * @param Client $tokenClient
     * @param CacheInterface $cacheClient
     */
    public function __construct(Client $tokenClient, CacheInterface $cacheClient)
    {
        $this->tokenClient = $tokenClient;
        $this->cacheClient = $cacheClient;
    }

    /**
     * @param Credential $credential
     * @return array
     * @throws GuzzleException
     */
    public function getToken(Credential $credential)
    {
        $key = $credential->createCacheKey();

        if ($this->cacheClient->has($key)) {
            return $this->cacheClient->get($key);
        }

        $token = $this->getTokenFromServer($credential);

        $this->cacheClient->set($key, $token, $token['expires_in']);

        return $token;
    }

    /**
     * @param Credential $credential
     * @return mixed
     */
    public function clearToken(Credential $credential)
    {
        $key = $credential->createCacheKey();

        return $this->cacheClient->del($key);
    }

    /**
     * @param Credential $credential
     * @return mixed
     * @throws GuzzleException
     */
    private function getTokenFromServer(Credential $credential)
    {
        $response = $this->tokenClient->post(
            self::TOKEN_URL,
            [
                'form_params' => [
                    'assertion' => $credential->createJwtToken(),
                    'grant_type' => self::GRANT_TYPE,
                ]
            ]
        );

        return json_decode($response->getBody(), true);
    }
}