<?php

namespace GcpRestGuzzleAdapter\Cache;

interface CacheInterface
{
    /**
     * @param $key
     * @return mixed
     */
    public function get($key);

    /**
     * @param $key
     * @param $value
     * @param $ttl
     * @return mixed
     */
    public function set($key, $value, $ttl = 0);

    /**
     * @param $key
     * @return mixed
     */
    public function has($key);

    /**
     * @param $key
     * @return mixed
     */
    public function del($key);
}