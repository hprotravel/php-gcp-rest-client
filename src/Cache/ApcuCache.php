<?php

namespace GcpRestGuzzleAdapter\Cache;

class ApcuCache implements CacheInterface
{
    /**
     * @inheritDoc
     */
    public function get($key)
    {
        return apcu_fetch($key);
    }

    /**
     * @inheritDoc
     */
    public function set($key, $value, $ttl = 0)
    {
        return apcu_store($key, $value, $ttl);
    }

    /**
     * @inheritDoc
     */
    public function has($key)
    {
        return apcu_exists($key);
    }

    /**
     * @inheritDoc
     */
    public function del($key)
    {
        return apcu_delete($key);
    }
}