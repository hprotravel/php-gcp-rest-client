<?php

namespace GcpRestGuzzleAdapter\Client;

use GcpRestGuzzleAdapter\Authentication\Credential;
use GcpRestGuzzleAdapter\Authentication\TokenProvider;
use GcpRestGuzzleAdapter\Cache\ApcuCache;
use GcpRestGuzzleAdapter\Middleware\AuthenticationMiddleware;
use GcpRestGuzzleAdapter\Middleware\RetryHandlerMiddleware;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;

class ClientFactory
{
    /**
     * @param string $email Service acount email
     * @param string $key Private Key
     * @param string $scope Project Scope
     * @param string $projectBaseUrl Full base url of Google Rest API
     * @param integer $maxRetries Maximum retry limit
     * @return Client
     */
    public static function createClient($email, $key, $scope, $projectBaseUrl, $maxRetries = 3)
    {
        $credential = new Credential($email, $key, $scope);

        $tokenProvider = new TokenProvider(new Client(), new ApcuCache());

        $handler = HandlerStack::create();

        $handler->push(new AuthenticationMiddleware($credential, $tokenProvider));

        $handler->push(new RetryHandlerMiddleware($maxRetries));

        return new Client(
            [
                'base_uri' => $projectBaseUrl,
                'handler' => $handler
            ]
        );
    }
}