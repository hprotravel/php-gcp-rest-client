<?php

namespace GcpRestGuzzleAdapter\Tests\Client;

use GcpRestGuzzleAdapter\Authentication\TokenSubscriber;
use GcpRestGuzzleAdapter\Client\ClientFactory;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class ClientFactoryTest extends TestCase
{
    public function testGetClient()
    {
        $client = ClientFactory::createClient(
            'test@test.com',
            'test_key',
            'test_scope',
            'http://www.google.com'
        );

        $this->assertInstanceOf(Client::class, $client);
    }
}