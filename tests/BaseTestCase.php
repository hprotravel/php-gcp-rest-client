<?php

namespace GcpRestGuzzleAdapter\Tests;

use GuzzleHttp\Client;
use GcpRestGuzzleAdapter\Authentication\Credential;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class BaseTestCase extends TestCase
{
    const TOKEN = [
        'token_type' => 'Bearer',
        'access_token' => 'test_token',
        'expires_in' => 3600
    ];

    /**
     * @return MockObject
     */
    protected function getGuzzleClientMock()
    {
        return $this
            ->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->getMock();
    }

    /**
     * @return Credential
     */
    protected function getCredentialFixture()
    {
        $sampleKey = openssl_pkey_new(
            [
                'digest_alg' => 'sha256',
                'private_key_bits' => 1024,
                'private_key_type' => OPENSSL_KEYTYPE_RSA
            ]
        );

        return new Credential('test@test.com', $sampleKey, 'test');
    }
}